# README #

The program is the solution for the problem to calculate the path of the movement of  robot on a Cartesian Graph to minimise the fuel consumption.

### How do I get set up? ###

if you have c++11 on your system then this compiles by issuing the command
 g++ RobotMotion.cpp -o RobotMotion

if c++11 is not present on the system and the c++ compiler you have is fairly new, then the command to compile the program is:

  g++ -std=c++0x RobotMotion.cpp -o RobotMotion

To run the program
 ./RobotMotion

### Contact ###

Rajesh Kumar Thakur
rajesh@csa.iisc.ernet.in
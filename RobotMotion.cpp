#include <queue>
#include "CartesianGraph.cpp"
#include "FloydWarshall.cpp"

class RobotMotion : public CartesianGraph, public FloydWarshall
{
  public:

  template<size_t rows_, size_t cols_>
  RobotMotion(int (&cGraph)[rows_][cols_]) : CartesianGraph(cGraph)
  {
     size = rows * cols;  
  }

  RobotMotion(int s, int n) : CartesianGraph(s, s, n)
  {
    size = s * s;
  }

  std::queue<int> path(int i, int j, int u, int v)
  {
    return FloydWarshall::path(i * CartesianGraph::rows + j, u * CartesianGraph::rows + v);
  } 

  void computeShortestPath()
  {
     createCostMatrix();
     FloydWarshall::cost_mat = CartesianGraph::cost_mat;
     compute(); 
  }

  void printPath(int i, int j , int u, int v)
  {
      std::queue<int> Q = path(i,j,u,v);
        int a;
        while(Q.empty() == false)
        {
          a=Q.front(); Q.pop();
        //convert to the base of Cartesian Graph 
        // pretty sloppy logic
        if(a<rows)
         std::cout<<"(0, "<<a <<") ";
        else
          std::cout<<"("<<a/rows <<", "<<a % rows <<") ";
        }
  }
};

int main()
{
 int b[9][9] = {{1,1,1,1,1,1,1,2,3},{1,1,1,1,2,2,1,1,2},{1,1,2,2,2,2,2,1,1},
    {1,1,1,2,3,3,2,1,2},{1,2,1,3,4,4,3,1,1},{1,1,1,3,6,5,2,3,1},
    {1,2,3,5,3,3,2,1,1},{1,1,2,3,2,2,1,1,2},{1,1,1,1,1,1,1,2,3}};
 // b is the input cartesian graph; 
 RobotMotion RM(b); 
 
 std::cout<<"The cartesian graph is : \n";
 RM.printCartesianGraph();

 std::cout<<std::endl;

 //The function below calculates the Shortest path using Floyd Warshall Algorithm
 RM.computeShortestPath();

 // Prints the shortest path from (0,4) to (8,4)
 std::cout<<"The Shortest Path from (0,4) to (8,4) is : \n";
 RM.printPath(0, 4, 8, 4);
 std::cout<<std::endl;


 //Testing on random Cartesian Graph of size 4 x 4 , the contour values in the graph is less tahn 10
 RobotMotion RM2(4,10); 
 
 std::cout<<"The cartesian graph is : \n";
 RM2.printCartesianGraph();

 std::cout<<std::endl;

 //The function below calculates the Shortest path using Floyd Warshall Algorithm
 RM2.computeShortestPath();

 // Prints the shortest path from (0,2) to (2,2)
 std::cout<<"The Shortest Path from (0,2) to (2,2) is : \n";
 RM2.printPath(0, 2, 2, 2);
 std::cout<<std::endl;

 return 0;
}


#include <iostream>
#include <limits>
#include <stdlib.h>
#include <math.h>
#include <queue>
#include <list>
#include <tuple>


class CartesianGraph{
  protected:
    int** cgraph; //cartesian graph as a 2-d matrix : size equals rows x cols
    // cost matrix may be used for floyd warshall algorithm
    int** cost_mat; //cost matrix which is built from the cartesian graph : size equals rows*cols x rows*cols 
    int rows, cols; // stores size: (rows, cols) of the cartesian graph

  public:
    CartesianGraph() //1st constructor does nothing 
    {
      cgraph = NULL;
      cost_mat = NULL;
    }

    template <size_t rows_, size_t cols_>
    CartesianGraph(int (&m)[rows_][cols_]) : rows(rows_), cols(cols_)
    {
      //This constructor needs a 2d matrix as input , and it creates a cartesian graph from the input matrix
      cgraph = new int*[rows];
    
      for(int k=0; k<rows; k++)
        cgraph[k] = new int[cols];
    
      for (int i = 0; i < rows; i++)
        for(int j = 0; j < cols; j++)
          cgraph[i][j]=m[i][j];
    
    }

    CartesianGraph(int h, int w, int n): rows(h), cols(w)
    {
      //This constructor creates a cartesian graph from of size h x w with random input range from 1..n
      cgraph = new int*[rows];
      
      for(int k=0; k<rows; k++)
        cgraph[k]= new int[cols];
      
      for (int i = 0; i < h; i++)
        for(int j = 0; j < w; j++)
          cgraph[i][j]=rand() % n;
      
    }

/* This method creats a cost matrix from a cartesian graph. The cost matriz resembles adjacency matrix where each cell is the
 * cost between node (i,j) and (i',j') the cost of moving from (i,j) to (i',j') is specified by the cost function.
 * Since this matrix is bigger than the cartesian graph. The logic used here to build the cost matrix is to map each row/col index
 * following a n-ary (base n number system) number representation. for e.g (0,1) in Cartesian Graph of size rows x rows maps to (0 * rows + 1) index of the 
 * cost matrix. Conversion between n-ary to cartesian co-ordinates are built in ithis function. */    
    void createCostMatrix()
      {
        int l, m;
        int V = rows * cols;
       
        // create a 2-d array, all rows first
        cost_mat = new int*[V];
        for(int k = 0; k < V; k++)
          cost_mat[k] = new int[V];

       //here the cols for te matrix is created
         for (int i = 0; i < V; i++)
          for(int j = 0; j < V; j++)
            cost_mat[i][j] = std::numeric_limits<std::int32_t>::max();

        //traverse the cartesian graph figure out its neighbors and then assign the cost 
        //to the cost matrix.
        for(int i = 0; i < rows; i++)
          for(int j = 0; j < cols; j++)
          {
            //uncomment line below to see the neighbors
         //   std::cout <<"\nNeighbors of (" <<i <<", "<<j<<"):" <<cgraph[i][j] <<" are : ";
         
            //   neighbors are stored in a queue
            std::queue<std::tuple<int, int> > Q = neighbors(std::make_tuple(i, j));
            std::tuple<int, int> a;

            //traverse till the neighbors queue is empty
            while(Q.empty() == false)
            {
              a=Q.front(); Q.pop();
              l = std::get<0>(a); m = std::get<1>(a);
             
              //cost of each neighbors updated with the cost function : abs(cgraph[i][j] - cgraph[l][m]) * 10 + 1
              cost_mat[i * rows + j][l * rows + m] = costFunction(cgraph[i][j], cgraph[l][m]);

              // debugging statement below, uncomment to see neighbors
              //std::cout<<"(" <<l <<", " <<m <<"):" <<cgraph[l][m] <<"\t";
            }
            //std::cout<<std::endl;
          }
     }
     
     // neighbors of current cell (i, j) of cartesian graph are determined, all diagonals are 
     // not included in the neighbprs queue. Only left, right, up, down neighbors.
     
      std::queue<std::tuple<int, int> > neighbors(std::tuple<int, int> v)
      {
        std::queue<std::tuple<int, int> > Q;
        int a,b;
        a = std::get<0>(v);
        b = std::get<1>(v);
        if(rows > 0)
        {
          for (int i = std::max(0,a-1); i <= std::min(a+1, rows-1) ; i++) {
            for (int j = std::max(0, b-1); j <= std::min(b+1,cols-1); j++) {
              if(a == i || b == j)
                if( i != a || j != b )
                  Q.push(std::make_tuple(i,j));
            }

          }
        }      
        return Q;
      }


    //cost function for movement along elevation n to elevation m
    int costFunction(int n, int m)
    {
      return (abs(n - m) * 10 + 1);
    }

/*
 * Print functions for debugging purposes
 * */

    void printCartesianGraph()
      {
        // Prints cartesian graph
        if(cgraph == NULL) {
          std::cout<<"Empty Cartesian Graph!! intialize graph \n"; return;
        }
        for (int i = 0; i < rows; i++)
        {
          for(int j = 0; j < cols; j++)
            std::cout<< cgraph[i][j] << "\t";
          std::cout<<std::endl;
        }
      }

      void printCostMatrix()
      {
        // Prints cost matrix
        if(cost_mat == NULL) {
          std::cout<<"Empty Matrix!! build cost_mat first \n"; return;
        }
        for (int i = 0; i < rows*cols; i++)
        {
          for(int j = 0; j < rows*cols; j++)
            std::cout<< cost_mat[i][j] << "\t";
          std::cout<<std::endl;
        }
      }

      void printNeighbors(std::tuple<int, int> v)
      {
        // Print neighbors of the cell (i,j)
        std::queue<std::tuple<int, int> > Q = neighbors(v);
        std::tuple<int, int> a;
        while(Q.empty() == false)
        {
          a=Q.front(); Q.pop();
          std::cout<<"("<<std::get<0>(a)<<", "<<std::get<1>(a)<<")\t";
        }
      }

};



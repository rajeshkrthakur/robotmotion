#include <stdlib.h>
#include <limits>
#include <iostream>
#include <queue>
class FloydWarshall {
  protected:
    int** cost_mat;
    int** next_mat;
    int size;
  public:
    FloydWarshall()
    {
      cost_mat = NULL;
      next_mat = NULL;
      size = 0;
    }

    template <size_t size_ >
      FloydWarshall(int (&costMatrix)[size_][size_]) : size(size_)
    {
      // initialize floydwarshall with a cost matrix of given size;
      cost_mat = new int*[size];

      for(int k=0; k<size; k++)
        cost_mat[k] = new int[size];

      for (int i = 0; i < size; i++)
        for(int j = 0; j < size; j++)
          cost_mat[i][j]=costMatrix[i][j];

    }

    void initialize()
    {
      next_mat = new int*[size];
      for(int k = 0; k < size; k++)
        next_mat[k] = new int[size];

      for(int i = 0; i < size; i++)
        for(int j = 0; j < size; j++)
          if(cost_mat[i][j] != std::numeric_limits<std::int32_t>::max() || cost_mat[i][j] != 0)
            next_mat[i][j] = j;
          else
            next_mat[i][j] = -1;

    }

    void compute()
    {
      //initialize next_matrix for path information
      initialize();

      for(int k = 0; k < size; k++)
      {
        for(int i = 0; i < size; i++)
          for(int j = 0; j < size; j++)
          {
            if(cost_mat[i][k] != std::numeric_limits<std::int32_t>::max() && cost_mat[k][j] != std::numeric_limits<std::int32_t>::max())
              if(cost_mat[i][j] > cost_mat[i][k] + cost_mat[k][j])
              {
                cost_mat[i][j] = cost_mat[i][k] + cost_mat[k][j];
                next_mat[i][j] = next_mat[i][k];
              }
          }
      }
    }

    std::queue<int> path(int u, int v)
    {
      // returns shortest path sequence from node i to node j


      std::queue<int> path;

      if(u < 0 || v < 0 || u >= size || v >= size)
      {
        std::cout<<"Invalid indices ";
        return path;
      }

      if(next_mat == NULL){ 
        std::cout<<"Empty Matrix!! run compute first \n"; return path;
      }

      if(next_mat[u][v] == -1) 
      {
        path.push(-1);
        //std::cout<<"Path is []\n";
      }
      path.push(u);
      //std::cout<<"Path with cost of "<<cost_mat[u][v]<<" is : "<<u;
      while(u != v)
      {
        u = next_mat[u][v];
        path.push(u);
        //std::cout<<" -> "<<u;
      }
      return path;
    }

    void printcost_mat()
    {
      if(cost_mat == NULL) {
        std::cout<<"Empty Matrix!! build cost_mat first \n"; return;
      }
      for (int i = 0; i < size; i++)
      {
        for(int j = 0; j < size; j++)
          std::cout<< cost_mat[i][j] << "\t";
        std::cout<<std::endl;
      }
    }

    void printnext_mat()
    {
      if(next_mat == NULL) {
        std::cout<<"Empty Matrix!! build next_mat first \n"; return;
      }
      for (int i = 0; i < size; i++)
      {
        for(int j = 0; j < size; j++)
          std::cout<< next_mat[i][j] << "\t";
        std::cout<<std::endl;
      }
    }
};
